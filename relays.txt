Ports:
	J1.P1 : DC source (+)
	J1.P23: DC sink (-)
	J1.P24: AC sink
	J1.P25: AC source
	J1.P26: NC?
	J1.R  : 3-phase R
	J1.S  : 3-phase S
	J1.T  : 3-phase T
	J1.BT1: 
	J1.BT2:
	J1.P34: sporo NO
	J1.P35: brzo NO
	J1.P36: dolje NO1
	J1.P37: gore NO1
	J1.P38: dolje NO2
	J1.P39: gore NO2

	J2.P1 : !X18 out
	J2.P2 : = J2.P1
	J2.P3 : brzo/sporo common, "KAB"
	J2.X  : governor fault?
	J2.P4 : vrata v. o. zatvorena

	J3.P11: AC out
	J3.P12: enable direction down
	J3.P13: enable direction up
	J3.P14: NC
	J3.P15-1: kabina zauzeta
	J3.P16: NC
	J3.P17: waiting in station = (vrata otvorena || nema smjer) && u stanici
	J3.P19: emergency stop?
	J3.P20: u stanici
	J3.P21: going up lamp
	J3.P22: going down lamp
	J3.P23: DC- out
	J3.P24: AC sink out
	J3.P31: X4 out
	J3.P32: debug: rqu
	J3.P33: debug: rqd
	J3.P40: AC out
	J3.P41: pozicija @kat0
	J3.P42: pozicija @kat1
	J3.P43: pozicija @kat2
	J3.P60: !X4 out
	J3.P61: odredište + lampica
	J3.P62: odredište + lampica
	J3.P63: odredište + lampica
	J3.P80: !X4 out
	J3.P81: poziv + lampica
	J3.P82: poziv + lampica
	J3.P83: poziv + lampica
	J3.P15-2: NC
	J3.P18: open doors trigger
	J3.P91: otvori vrata
	J3.P92: too_heavy?
	J3.P93: zatvori vrata
	J3.P94: aux logic
	J3.P95: aux logic
	J3.P96: aux logic
	J3.P97: aux logic
	J3.P98: aux logic
	J3.P99-1: bridge1
	J3.P99-2: bridge1

	J4    : non-existent, just helper for referencing the supply net


Rendering:
	./reving-nl2ladder -S J3-Port-P11 -S J4-Port-1 -s J1-Port-P24 -s J1-Port-P23 lift4.json ladder4.pdf

Bridges:
	P3.P11 - P3.P14
	P3.P62 - P3.P82

Descriptions:
	X5  = enable direction up
	X6  = enable direction down
	X7  = too heavy?
	X8  = idi brzo
		= vrata zatvorena && isteko X43 && ima trigger za smjer
	X9  = X8
	X13 = X26
	X14 = idi sporo
	    = vrata zatvorena && ima smjer && gotovo brzo kretanje
	X16 = RST (3-phase) present
	X17 = vrata zatvorena && ima smjer
	X19 = vrata zatvorena
	X20 = gore/dolje
	X21 = X20
	X22 = trigger za dolje
	X23 = trigger za gore
	X24 = gore
	X25 = X24
	X27 = reaching destination (current floor matches a requested floor, or a user pressed the button on the same floor)
	X28 = u stanici
	X29 = destA kat2
	X30 = destA kat1
	X31 = destA kat0
	X32 = destB kat0
	X33 = destB kat1
	X34 = destB kat2
	X35 = delayed X41 (kat2)
	X37 = delayed X40 (kat1)
	X38 = delayed X39 (kat0)
	X39 = pozicija=kat0
	X40 = pozicija=kat1
	X41 = pozicija=kat2
	X42 = timer za zatvaranje vrata
	X43 = timer za pokretanje
		= vrata zatvorena && nema aktivan smjer


Rules:
	rqu =                         (X29) || (X30 && !X37 && !X35)
	    = destA@2 || (destA@1 && @kat0)
	rqd = (X16 && !X26 && X32) || (X31) || (X30 && !X37 && !X38)
	    = (have_RST && ? && destB@0) || destA@0 || (destA@1 && !@kat2)
	en1 = (!X4 && !X43) || (X16 &&  X26)
	    = (!stop && !go_timer) || (have_RST && X26) 
	en2 = ( X3 && !X28) || (!X4 && !X26)
	    = (stop_req && in_station) || (!stop && !too_fast)
	wis = !X17 && X28 // waiting in station
	    = (vrata otvorena || nema aktivan smjer) && u_stanici
	X2  = !X11 && trig(rqd, en1)
	X3  = trig(
	           (X41 && (X29 || X34 && X11)) ||
	           (X39 && (X31 || X32 && X10)) ||
	           (X40 && (X30 || X33 && X10)),
		       !X4 && !X26 && !X28)
	    = trig(
	           (@kat2 && (destA@2 || destB@2 && going_up  )) ||
	           (@kat1 && (destA@1 || destB@1 && going_down)) ||
			   (@kat0 && (destA@0 || destB@0 && going_down)),
	           !X4 && !X26 && !u_stanici)
	    = stop_req
	X4  = J3.P19
	X5  = J3.P13
	X6  = J3.P12
	X7  = J3.P92
	X8  = trig(X43, !X4) && (X22 || X23) && X19
	    = go_fast
	X9  = X8
	X10 = X2
	    = going_down
	X11 = (((en1 && X1) || rqu) && !X10)
	    = going_up
	X12 = trig(X31 || X29 || X30, en2)
	    = trig(any destA, en2)
	X13 = X26
	X14 = X19 && (X20 || X24) && !X8
	    = go_slow
	X15 = J3.P94
	X16 = (dvije faze prisutne)
	X17 = X19 && (X20 || X24)
	X18 = wis && X27
	    = waiting_in_station && reaching_dest
		= open doors trigger
	X19 = J2.P4
	X20 = X16 && !X26 && (!X28 || X8 || X4) && trig(X22, !X4) && !X24
	X21 = X20
	X22 = rqd && !X38 && X10 && X6 && !X23 && !X3
	    = rqd && !@kat1 && going_down && enable_down && !trig_up && !stop_req
	    = trig_down
	X23 = rqu && !X35 && X11 && X5 && !X22 && !X3
	    = rqu && !@kat2 && going_up && enable_up && !trig_down && !stop_req
	    = trig_up
	X24 = X16 && !X26 && (!X28 || X8 || X4) && trig(X23, !X4) && !X20
	X25 = X24
	X26 = trig(J2.X, !X12 && !X18)
	X27 = (X39 && (X31 || X32)) ||
	      (X41 && (X29 || X34)) ||
	      (X40 && (X30 || X33))
	    = (@kat0 && (destA@0 || destB@0) ||
	      (@kat1 && (destA@1 || destB@1) ||
	      (@kat2 && (destA@2 || destB@2)
	    = reaching_dest
	X28 = J3.P20
	X42 = wis && !X18 && !X7
		= wis && !(wis && X27) && !X7
		= wis && !X27 && !X7
		= wis && !reaching_dest && !heavy
	X43 = X19 && !X17
	

	*** Za katove ***
	X29 = !X35 && trig(J3.P63, en2)		# destA@2
	X30 = !X37 && trig(J3.P62, en2)		# destA@1
	X31 = !X38 && trig(J3.P61, en2)		# destA@0

	X34 = !X35 && trig(J3.P83, !X4)		# destB@2
	X33 = !X37 && trig(J3.P82, !X4)		# destB@1
	X32 = !X38 && trig(J3.P81, !X4)		# destB@0

	X39 = J3.P41						# @kat0
	X40 = J3.P42						# @kat1
	X41 = J3.P43						# @kat2

	X38 = J3.P41 && X39					# @kat0
	X37 = J3.P42 && X40					# @kat1
	X35 = J3.P43 && X41					# @kat2


Note:
	trig(t) = self || t
	trig(t, LE) = (self && LE) || t		# trigger forces turn on (LE = latch enable)
	x && trig(t) = x && (self || t)		# trigger can't turn on without x
	# read like this: t can turn on the relay, but to keep it on LE must be true
