struct inp {
  int pin;
  const char *name;
  bool prev;
};

inp ins[] = {
  {A3, "slow"},
  {A2, "fast"},
  {A1, "up"},
  {A0, "down"},
  {12, "open"},
  {11, "close"}
};
const byte num_ins = sizeof(ins) / sizeof(*ins);
const byte min_out = 3;
const byte max_out = 9;

void setup() {                
  for (byte i = 3; i < 10; ++i)
    pinMode(i, OUTPUT);
  for (byte i = 0; i < num_ins; ++i) {
    pinMode(ins[i].pin, INPUT_PULLUP);
    ins[i].prev = !digitalRead(ins[i].pin);
  }
  Serial.begin(115200);
}

void loop() {
  int c = Serial.read();
  int p;
  switch (c) {
    case 's':
      digitalWrite(8, LOW);
      Serial.println(F("not in station"));
      break;
    case 'S':
      digitalWrite(8, HIGH);
      Serial.println(F("in station"));
      break;
    case 'p':
      digitalWrite(3, LOW);
      break;
    case 'P':
      digitalWrite(3, HIGH);
      break;
    case '1':
      digitalWrite(7, HIGH);  // 1
      digitalWrite(5, LOW);  // 2
      digitalWrite(6, LOW);  // 3
      Serial.println(F("1st floor"));
      break;
    case '2':
      digitalWrite(7, LOW);  // 1
      digitalWrite(5, HIGH);  // 2
      digitalWrite(6, LOW);  // 3
      Serial.println(F("2nd floor"));
      break;
    case '3':
      digitalWrite(7, LOW);  // 1
      digitalWrite(5, LOW);  // 2
      digitalWrite(6, HIGH);  // 3
      Serial.println(F("3rd floor"));
      break;
    case 'f':
      digitalWrite(7, LOW);  // 1
      digitalWrite(5, LOW);  // 2
      digitalWrite(6, LOW);  // 3
      Serial.println(F("no floor"));
      break;
    case 'd':
      digitalWrite(9, LOW);
      Serial.println(F("doors open"));
      break;
    case 'D':
      digitalWrite(9, HIGH);
      Serial.println(F("doors closed"));
      break;
  }    
  for (byte i = 0; i < num_ins; ++i) {
    bool state = digitalRead(ins[i].pin);
    if (state != ins[i].prev) {
      Serial.print(ins[i].name);
      Serial.print(' ');
      Serial.println(state);
      ins[i].prev = state;
    }
  }
}
